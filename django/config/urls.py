from django.conf import settings
from django.conf.urls import include, url
from django.conf.urls.static import static
from django.contrib import admin

urlpatterns = [
    url(r'^titok/', include(admin.site.urls)),
    url(r'^markitup/', include('markitup.urls')),
    url(r'^', include('pylvax_app.urls')),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

