import CommonMark
# from .shortcodes import process_all_shortcodes


def commonmark_render(input):
    parser = CommonMark.DocParser()
    renderer = CommonMark.HTMLRenderer()
    ast = parser.parse(input)
    html = renderer.render(ast)
    return html


def process_markdown(input):
    md_rendered = commonmark_render(input)
    # md_processed = process_all_shortcodes(md_rendered, {})
    return md_rendered
