# -*- coding: utf-8 -*-

import os
from django.db import models
from markitup.fields import MarkupField


def gallery_kepek_filename(instance, filename):
        _, ext = os.path.splitext(filename)
        ext = ext.lower()
        basename = instance.slug
        dir = 'gallery_kepek'
        return os.path.join(dir, '{}{}'.format(basename, ext))


def blog_kepek_filename(instance, filename):
        _, ext = os.path.splitext(filename)
        ext = ext.lower()
        basename = instance.slug
        dir = 'blog_kepek'
        return os.path.join(dir, '{}{}'.format(basename, ext))


class MenuItem(models.Model):
    slug = models.SlugField(unique=True)
    text = models.CharField(max_length=100)
    order = models.IntegerField(null=True, blank=True)

    def __unicode__(self):
        return self.text

    class Meta:
        ordering = ['order']


class HomeItem(models.Model):
    slug = models.SlugField(unique=True)
    url = models.CharField(max_length=40)
    szoveg = MarkupField()
    order = models.IntegerField(null=True, blank=True)

    def __unicode__(self):
        return self.slug

    class Meta:
        ordering = ['order']



class Tartalom(models.Model):
    slug = models.SlugField(unique=True)
    szoveg = MarkupField(blank=True)

    def __unicode__(self):
        return self.slug


class Feladat(models.Model):
    datum = models.DateField(auto_now_add=False)
    slug = models.SlugField(unique=True)
    cim = models.CharField(max_length=100)
    leiras = MarkupField(blank=True)
    megoldas = MarkupField(blank=True)

    def __unicode__(self):
        return self.cim

    class Meta:
        ordering = ['-datum']


class Tematika(models.Model):
    datum = models.DateField(auto_now_add=False)
    slug = models.SlugField(unique=True)
    cim = models.CharField(max_length=100)
    leiras = MarkupField(blank=True)

    def __unicode__(self):
        return self.cim

    class Meta:
        ordering = ['-datum']



class Kepek(models.Model):
    slug = models.SlugField(unique=True)
    kep_file = models.FileField(upload_to=gallery_kepek_filename, blank=True)

    def __unicode__(self):
        return self.slug


class Blog(models.Model):
    datum = models.DateTimeField(auto_now_add=True)
    cim = models.CharField(max_length=100)
    leiras = MarkupField(blank=True)
    kep = models.FileField(upload_to=blog_kepek_filename, blank=True)

    def __unicode__(self):
        return self.cim

    class Meta:
        ordering = ['-datum']

