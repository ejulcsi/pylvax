from django.shortcuts import render, get_object_or_404
from .models import MenuItem, Tartalom, Feladat, Kepek, Tematika, HomeItem


def home(request):
    menu_list = MenuItem.objects.order_by('order')
    home_list = HomeItem.objects.order_by('order')
    context_dict = {
        'menu_list': menu_list,
        'active': 'home',
        'home_list': home_list
    }

    return render(request, 'pylvax_app/home.html', context_dict)


def about(request):
    menu_list = MenuItem.objects.order_by('order')
    home_list = HomeItem.objects.order_by('order')
    context_dict = {
        'menu_list': menu_list,
        'active': 'about',
        'home_list': home_list
    }

    return render(request, 'pylvax_app/about.html', context_dict)


def task_item(request, task_slug):
    menu_list = MenuItem.objects.order_by('order')
    task = get_object_or_404(Feladat, slug=task_slug)

    context_dict = {
        'menu_list': menu_list,
        'active': 'tasks',
        'task': task,
    }

    return render(request, 'pylvax_app/task_item.html', context_dict)


def tasks(request):
    menu_list = MenuItem.objects.order_by('order')
    task_list = Feladat.objects.order_by('-datum')

    context_dict = {
        'menu_list': menu_list,
        'active': 'tasks',
        'task_list': task_list,
    }

    return render(request, 'pylvax_app/task_list.html', context_dict)


def newbie(request):
    menu_list = MenuItem.objects.order_by('order')
    new = Tartalom.objects.get(slug='newbie')

    context_dict = {
        'menu_list': menu_list,
        'active': 'newbie',
        'new': new,
    }

    return render(request, 'pylvax_app/newbie.html', context_dict)


def info(request):
    menu_list = MenuItem.objects.order_by('order')
    info_content = Tartalom.objects.get(slug='info')

    context_dict = {
        'menu_list': menu_list,
        'active': 'info',
        'info_content': info_content,
    }

    return render(request, 'pylvax_app/info.html', context_dict)


def topic_item(request, topic_slug):
    menu_list = MenuItem.objects.order_by('order')
    topic = get_object_or_404(Tematika, slug=topic_slug)

    context_dict = {
        'menu_list': menu_list,
        'active': 'topics',
        'topic': topic,
    }

    return render(request, 'pylvax_app/topic_item.html', context_dict)


def topics(request):
    menu_list = MenuItem.objects.order_by('order')
    topic_list = Tematika.objects.order_by('-datum')

    context_dict = {
        'menu_list': menu_list,
        'active': 'topics',
        'topic_list': topic_list,
    }

    return render(request, 'pylvax_app/topic_list.html', context_dict)


def contact(request):
    menu_list = MenuItem.objects.order_by('order')

    context_dict = {
        'menu_list': menu_list,
        'active': 'contact',
    }

    return render(request, 'pylvax_app/contact.html', context_dict)


def pics(request):
    menu_list = MenuItem.objects.order_by('order')
    pic_list = Kepek.objects.all()

    context_dict = {
        'menu_list': menu_list,
        'active': 'pics',
        'pic_list': pic_list,
    }

    return render(request, 'pylvax_app/pics.html', context_dict)
