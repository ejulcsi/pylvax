from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^$', views.home, name='home'),
    url(r'^about/$', views.about, name='about'),
    url(r'^newbie/$', views.newbie, name='newbie'),
    url(r'^contact/$', views.contact, name='contact'),
    url(r'^tasks/$', views.tasks, name='tasks'),
    url(r'^tasks/(?P<task_slug>[\w-]+)/$', views.task_item, name='task'),
    url(r'^info/$', views.info, name='info'),
    url(r'^pics/$', views.pics, name='pics'),
    url(r'^topics/$', views.topics, name='topics'),
    url(r'^topics/(?P<topic_slug>[\w-]+)/$', views.topic_item, name='topic_item'),
]
