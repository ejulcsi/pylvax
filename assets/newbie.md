First steps
==================
Pay attention to your assignments, practice at home, talk with developers and you'll see that your skills will improve sooner than you would think!

You are gonna need to install the following stuffs for getting started:

**Windows**

**Python** (use 32-bit)</br>
[https://www.python.org/ftp/python/2.7.9/python-2.7.9.msi](http://)

**Sublime Text 2** (use 32-bit)</br>
[http://c758482.r82.cf2.rackcdn.com/Sublime%20Text%202.0.2%20Setup.exe](http://)</br></br>
Total Commander</br>
[http://totalcmd2.s3.amazonaws.com/tcmd851ax32_64.exe](http://)</br></br>

**Cmder**</br>
[https://github.com/bliker/cmder/releases/download/v1.1.4.1/cmder.zip](http://)</br></br>

**Path Editor**</br>
[http://rix0r.nl/downloads/windowspatheditor/windowspatheditor-1.3.zip](http://)
</br></br>

**Ipython**</br>
ipython‑3.0.0‑py27‑none‑any.whl</br>
[http://www.lfd.uci.edu/~gohlke/pythonlibs/#ipython](http://)
</br></br>
