Beautiful Soup


```
from bs4 import BeautifulSoup
import codecs


def read_file_contents(path):
    with codecs.open(path, encoding='utf-8') as infile:
        return infile.read().strip()

kiralyok_str = read_file_contents('kiralyok.html')


soup = BeautifulSoup(kiralyok_str, 'html.parser')

h3_list = soup.find_all('h3')
h3 = h3_list[0]
kiralyhaz = h3.find('span').text

korszakok = list()

for h3 in h3_list:
    span = h3.find('span', {'class': 'mw-headline'})
    if span:
        korszak = span.text
        table = h3.next_sibling.next_sibling

        korszakok.append((korszak, table))

```
