Hello!
------
Welcome! Pylvax is a workshop, where you can learn coding even if you are totally beginner and haven't seen any lines of code before. </br>
We meet every wednesday, we code together and share our experiences with each other.

Don't know how to code? No problem!
-----------------------------
We don't expect you to have any previous experience in coding or in the IT field.<br>
You can take the first steps even at home!

You can also help!
-------------
If you are a coding ninja, or a developer with some experiences, you are also welcome. <br>
Give lessons, help the beginners, and perhaps you will also learn something new!


What we learn
-------------
We are learning in two separated groups, Dániel Ábel helps to the newcomes to get into the flow.</br>
Zsolti Erő shows more and more magic to the old hands.</br>
The details you can find here.


What we did so far
---------------------
You can find here the materials of past lessons and homeworks. 


What else?
--------------
You can continue learning at home, we collected useful stuffs, great tutorials and books.
