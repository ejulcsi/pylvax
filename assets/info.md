<p class="info_title">Here we collected many information for you to help learning.</p>
</br>
<p class="y_line">
Intro to Computer Science
</p>
First an amazing course, which explains the very basics of programming and Python language:</br>

[www.udacity.com/course/cs101](https://www.udacity.com/course/cs101)

<p class="b_line">
Codecademy / Python</p>

Codecademy is useful to practice the newly learned stuffs.</br>
[www.codecademy.com/tracks/python](https://www.codecademy.com/tracks/python)

<p class="y_line">
Learn Python the hard way</p>

A great online book teaches you from fundamentals, shows how to use command line, and helps to take the first steps.</br>
 The language it is written, awesome! :)</br>
[learnpythonthehardway.org/book/](http://learnpythonthehardway.org/book/)

<p class="b_line">Programming Foundations with Python</p>

If you want to know more about object oriented programming, you can start with simple but great exercises.</br>
[www.udacity.com/course/ud036](http://www.udacity.com/course/ud036)

<p class="y_line">Exercism</p>

The idea behind the site is pretty simple: write code for all kinds of problems, submit it, get feedback for it from other users, and give feedback to them. You'll get to see how others solve problems.</br>
[exercism.io](http://exercism.io/)

<p class="b_line">Intro to Git and Github</p>
This course can teach you to understand version control and shows how to collaborate with other developers.</br>

[www.udacity.com/course/how-to-use-git-and-github](http://www.udacity.com/course/how-to-use-git-and-github--ud775)

<p class="y_line">
An Introduction to Interactive Programming in Python</p>
It is  partially enabled course, you have to check, when it is opened, but also good, and from basics.</br>

[www.coursera.org/course/interactivepython1
](http://www.coursera.org/course/interactivepython1)

It has a second part as well:</br>
[www.coursera.org/course/interactivepython2](http://www.coursera.org/course/interactivepython2)
