#!/usr/bin/env python

import sys

from fabric.tasks import execute
from fabric.api import env, settings, sudo, run

from maphub_fabric.pkg import pkg_mozjpeg, pkg_node
from maphub_fabric.postgresql import postgresql_create_user
from maphub_fabric.web import web_create_site
from maphub_fabric.utils import chown_home_folder

from pylvax_fabric.config import WEBAPP_NAME, LOGS_DIR, MEDIA_DIR, \
    STATIC_DIR, APP_DIR, GIT_URL, PORT
from pylvax_fabric.backuprestore import db_backup, db_restore, media_backup, media_restore
from pylvax_fabric.deploy import deploy  # noqa

env.host_string = 'iwzsolti'


def prepare():
    web_create_site(WEBAPP_NAME, static=False, django=True, supervisord_port=PORT)
    postgresql_create_user(WEBAPP_NAME)
    pkg_mozjpeg()
    pkg_node()

    with settings(sudo_user=WEBAPP_NAME):
        sudo('mkdir -p {}'.format(LOGS_DIR))
        sudo('mkdir -p {}'.format(MEDIA_DIR))
        sudo('mkdir -p {}'.format(STATIC_DIR))

    git_clone()
    chown_home_folder(WEBAPP_NAME)


def restore():
    db_restore()
    media_restore()


def backup():
    db_backup()
    media_backup()


def git_clone():
    run('rm -rf {}'.format(APP_DIR))
    run('git clone {} {}'.format(GIT_URL, APP_DIR))



if len(sys.argv) != 2:
    sys.exit('argument missing')

execute(globals()[sys.argv[1]])
