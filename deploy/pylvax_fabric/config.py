WEBAPP_NAME = 'pylvax.com'

GIT_URL = 'git@bitbucket.org:ejulcsi/pylvax.git'
PORT = 53737

WEB_DIR = '/usr/home/{}/web'.format(WEBAPP_NAME)

APP_DIR = WEB_DIR + '/app'
LOGS_DIR = WEB_DIR + '/logs'
MEDIA_DIR = WEB_DIR + '/media'
STATIC_DIR = WEB_DIR + '/static'

DJANGO_DIR = APP_DIR + '/django'
DB_DIR = APP_DIR + '/db'
DEPLOY_DIR = APP_DIR + '/deploy'
VENV_BIN_DIR = APP_DIR + '/venv/bin'
NODE_BIN_DIR = DEPLOY_DIR + '/node_modules/.bin'

