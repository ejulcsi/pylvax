import time

from fabric.api import sudo, run, settings, cd, shell_env

from maphub_fabric.utils import chown_home_folder
from maphub_fabric.supervisord import supervisord_django

from .config import WEBAPP_NAME, LOGS_DIR, STATIC_DIR, APP_DIR, \
    VENV_BIN_DIR, DJANGO_DIR, DEPLOY_DIR, NODE_BIN_DIR, PORT


def deploy():
    install_pkgs()
    supervisord_django(WEBAPP_NAME, PORT)

    website_down()
    downtime_start = time.time()

    git_update(APP_DIR)
    install_pip_dependencies()
    clean_old_files()

    chown_home_folder(WEBAPP_NAME)
    django_prepare()

    website_up()
    downtime_end = time.time()

    print 'Total downtime: {} seconds'.format(downtime_end - downtime_start)


def install_pkgs():
    run('pkg install -y ImageMagick pngquant advancecomp')


def website_down():
    run('supervisorctl stop {}'.format(WEBAPP_NAME))


def website_up():
    run('supervisorctl update')
    run('supervisorctl start {}'.format(WEBAPP_NAME))
    run('service nginx reload')


def git_update(dir):
    with cd(dir):
        run('git fetch origin')
        run('git reset --hard origin/master')


def install_pip_dependencies():
    with cd(APP_DIR):
        run('git clean -xdf')
        run('virtualenv venv')

        # pip
        server_time_now = run('date +"%F-%H%M%S"')
        run('{}/pip install -r requirements/production.txt'.format(VENV_BIN_DIR))
        run('mkdir -p {}/pip'.format(LOGS_DIR))
        run('{}/pip freeze > {}/pip/{}.txt'.format(VENV_BIN_DIR, LOGS_DIR, server_time_now))


def clean_old_files():
    run('rm -rf {}/*'.format(STATIC_DIR))
    run('rm -f {}/django.log'.format(LOGS_DIR))
    run('rm -f {}/nginx-*.log'.format(LOGS_DIR))
    run('rm -f {}/supervisord-*.log'.format(LOGS_DIR))


def django_prepare():
    stylesheet_path = 'static/css/stylesheet.css'

    with settings(sudo_user=WEBAPP_NAME):
        with cd(DEPLOY_DIR):
            sudo('npm install')

        with cd(DJANGO_DIR), shell_env(DJANGO_SETTINGS_MODULE='config.settings.production'):
            sudo('{}/autoprefixer-cli {}'.format(NODE_BIN_DIR, stylesheet_path))
            sudo('{}/cleancss -o {stylesheet} --s0 {stylesheet}'.format(NODE_BIN_DIR, stylesheet=stylesheet_path))
            sudo('{}/python manage.py makemigrations'.format(VENV_BIN_DIR))
            sudo('{}/python manage.py migrate'.format(VENV_BIN_DIR))
            sudo('{}/python manage.py collectstatic --link --noinput'.format(VENV_BIN_DIR))

