#!/usr/bin/env bash

DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

VENV_NAME=simanjo

(cd "$DIR/../.." || exit
 deactivate
 rmvirtualenv $VENV_NAME
 mkvirtualenv $VENV_NAME
 pip install -r requirements/local.txt)

